/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/*#include "RC.h"
#include "bsp_can.h"
#include "pid.h"
#include "Remote_Control.h"
#include "main.h"
#include "stm32f4xx_hal.h"
#include "can.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"*/
#include "pid.h"
#include <stdio.h>
#include <stdlib.h>
//#define uint8_t   unsigned char
extern float test[8];
extern float Bchang;//��̨����ֵ�������Ƕȣ�
extern float USAR_6623Angle_init;//1400-3000
extern int Taget_X;
extern int Taget_Y;
extern unsigned char DATA_BUFFER2[14];
extern unsigned char  SJ_X[4];
extern unsigned char  SJ_Y[4];
extern   unsigned char  STA_USART6;
extern float  OUTput_X;
extern float  OUTput_Y;
#define X_mid   320
#define Y_mid   240
extern  float ch[14];

extern     CAN_HandleTypeDef hcan1;
//extern    struct XYZ
//{
//	
//	/*
//uint16_t   Pich;//X
//	uint16_t    Roll;//Y
//	uint16_t    Yaw;//Z
//	*/
//	float   Pich;//X
//	float    Roll;//Y
//	float    Yaw;//Z
//			float    TEM;//Z
//}XYZ;
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define KEY_Pin GPIO_PIN_10
#define KEY_GPIO_Port GPIOD
#define LED1_Pin GPIO_PIN_14
#define LED1_GPIO_Port GPIOF
#define LED2_Pin GPIO_PIN_7
#define LED2_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
