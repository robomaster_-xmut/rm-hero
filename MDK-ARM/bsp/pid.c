/**
  ******************************************************************************
  * @file    pid.c
  * @author  Ginger
  * @version V1.0.0
  * @date    2015/11/14
  * @brief   对每一个pid结构体都要先进行函数的连接，再进行初始化
  ******************************************************************************
  * @attention 应该是用二阶差分(d)云台会更加稳定
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "pid.h"
#include "stm32f4xx.h"
#include "math.h"
#include "main.h"
#define ABS(x)		((x>0)? x: -x) 



PID_TypeDef pid_pitch,pid_pithch_speed,pid_roll,pid_roll_speed,pid_yaw_speed;
extern int isMove;

/*参数初始化--------------------------------------------------------------*/
static void pid_param_init(
	PID_TypeDef * pid, 
	PID_ID   id,
	uint16_t maxout,
	uint16_t intergral_limit,
	float deadband,
	uint16_t period,
	int16_t  max_err,
	int16_t  target,

	float 	kp, 
	float 	ki, 
	float 	kd)
{
	pid->id = id;		
	
	pid->ControlPeriod = period;             //没用到
	pid->DeadBand = deadband;
	pid->IntegralLimit = intergral_limit;
	pid->MaxOutput = maxout;
	pid->Max_Err = max_err;
	pid->target = target;
	
	pid->kp = kp;
	pid->ki = ki;
	pid->kd = kd;
	
	pid->output = 0;
}

/*中途更改参数设定--------------------------------------------------------------*/
static void pid_reset(PID_TypeDef * pid, float kp, float ki, float kd,PID_ID   id)
{
	pid->kp = kp;
	pid->ki = ki;
	pid->kd = kd;
	pid->id = id;
}

/*pid计算-----------------------------------------------------------------------*/

	
static float pid_calculate(PID_TypeDef* pid, float measure)//, int16_t target)
{
//	uint32_t time,lasttime;
	
	if(pid->id == PID_Speed)/////////////////////////////////////////////////////
	{
	pid->lasttime = pid->thistime;
	pid->thistime = HAL_GetTick();
	pid->dtime = pid->thistime-pid->lasttime;
	pid->measure = measure;
  //	pid->target = target;	
	pid->last_err  = pid->err;
	pid->last_output = pid->output;	
	pid->err = pid->target - pid->measure;

	//是否进入死区
	if((ABS(pid->err) > pid->DeadBand))
	{				
		pid->pout = pid->kp * pid->err;
		pid->iout += (pid->ki * pid->err);
		pid->dout =  pid->kd * (pid->err - pid->last_err); 		
		//积分是否超出限制
		if(pid->iout > pid->IntegralLimit)
			pid->iout = pid->IntegralLimit;
		if(pid->iout < - pid->IntegralLimit)
			pid->iout = - pid->IntegralLimit;
		
		//pid输出和
		pid->output = pid->pout + pid->iout + pid->dout;
		

		//pid->output = pid->output*0.7f + pid->last_output*0.3f;  //滤波？
		if(pid->output>pid->MaxOutput)         
		{
			pid->output = pid->MaxOutput;
		}
		if(pid->output < -(pid->MaxOutput))
		{
			pid->output = -(pid->MaxOutput);
		}
	
	}}
	
		else if(pid->id == PID_Position)/////////////////////////////////////////////////////
	{
	if(pid->target<0)pid->target=8191+pid->target;
		
	pid->lasttime = pid->thistime;
	pid->thistime = HAL_GetTick();
	pid->dtime = pid->thistime-pid->lasttime;
	pid->measure = measure;
	pid->last_err  = pid->err;
	pid->last_output = pid->output;
	pid->err = pid->target - pid->measure;
		
	if( pid->err>4096){  pid->err= pid->err-8191;}
	else if( pid->err<-4096){ pid->err= pid->err+8191;}

		
		pid->pout = pid->kp * pid->err;
		pid->iout += pid->ki * pid->err;
		pid->dout = pid->kd * (pid->err - pid->last_err); 
	
	
		//积分是否超出限制
			if(pid->iout > pid->IntegralLimit)
				 pid->iout = pid->IntegralLimit;
			if(pid->iout < - pid->IntegralLimit)
				 pid->iout = - pid->IntegralLimit;
		     pid->output = pid->pout + pid->iout + pid->dout;
			if(pid->output>pid->MaxOutput)         
			{
				pid->output = pid->MaxOutput;
			}
			if(pid->output < -(pid->MaxOutput))
			{
				pid->output = -(pid->MaxOutput);
			}
	
	
	
	}


	return pid->output;
}

/*pid结构体初始化，每一个pid参数需要调用一次-----------------------------------------------------*/
void pid_init(PID_TypeDef* pid)
{
	pid->f_param_init = pid_param_init;
	pid->f_pid_reset = pid_reset;
	pid->f_cal_pid = pid_calculate;
}
void PID_init(void)
	{
  for(int i=0; i<4; i++)
  {
    pid_init(&motor_pid[i]);
    motor_pid[i].f_param_init(&motor_pid[i],//struct _PID_TypeDef *pid,  //PID参数初始化d
		PID_Speed,//  PID_ID id,
		16384,//	   uint16_t maxOutput,
		5000,//			   uint16_t integralLimit,
		10,//   float deadband,
		0,// uint16_t controlPeriod,
		8000,//		int16_t max_err,
		0,//	int16_t  target,
		1.5,//p
		0.1,//i
		0);//d
		
  }
   pid_init(&motor_pid[4]);
    motor_pid[4].f_param_init(&motor_pid[4],//struct _PID_TypeDef *pid,  //PID参数初始化d
		PID_Speed,//  PID_ID id,
		16384,//	   uint16_t maxOutput,
		5000,//			   uint16_t integralLimit,
		10,//   float deadband,
		0,// uint16_t controlPeriod,
		8000,//		int16_t max_err,
		0,//	int16_t  target,
		3,//p
		0.1,//i
		0);//d
pid_init(&motor_pid[5]);motor_pid[5].f_param_init(&motor_pid[5],//struct _PID_TypeDef *pid,  //PID参数初始化d
		PID_Position,//  PID_ID id,
		30000,//	   uint16_t maxOutput,
		30000,//			   uint16_t integralLimit,
		0,//   float deadband,
		0,// uint16_t controlPeriod,
		8192,//		int16_t max_err,    没用到
		0,//	int16_t  target,
		20,//p
		0,//i
		0);//d
		
		for(int i=7; i<12; i++)
  {
    pid_init(&motor_pid[i]);
    motor_pid[i].f_param_init(&motor_pid[i],//struct _PID_TypeDef *pid,  //PID参数初始化d
		PID_Speed,//  PID_ID id,
		16384,//	   uint16_t maxOutput,
		5000,//			   uint16_t integralLimit,
		10,//   float deadband,
		0,// uint16_t controlPeriod,
		8000,//		int16_t max_err,
		0,//	int16_t  target,
		1.5,//p
		0.1,//i
		0);//d
		
  }
	
	pid_init(&motor_pid[12]);motor_pid[12].f_param_init(&motor_pid[12],//struct _PID_TypeDef *pid,  //PID参数初始化d
		PID_Position,//  PID_ID id,
		30000,//	   uint16_t maxOutput,
		30000,//			   uint16_t integralLimit,
		0,//   float deadband,
		0,// uint16_t controlPeriod,
		8192,//		int16_t max_err,    没用到
		0,//	int16_t  target,
		170,//p
		0.01,//i
		0);//d
	
	}
		 float X_PI (int Target_X)   //  Target：目标在视觉中的坐标     
{ 	
	
   float Kp=1,Ki=0.0001,Kd=0.5;	
	 static int err,Last_err=0;
	static float I_out=0;
		float out_put;
	 err=X_mid-Target_X;                //计算偏差
I_out+=Ki*err;
	 out_put=Kp*err+I_out+Kd*(err-Last_err);   //增量式PI控制器
	 Last_err=err;	                   //保存上一次偏差 
	
	if(out_put>1000)
		out_put=1000;
	else if (out_put<-1000)
		out_put=-1000;
	
	 return out_put;                         //增量输出
	
}
		 float Y_PI (int Target_Y)   //  Target：目标在视觉中的坐标   1400-3000  
{ 	
	
   float Kp=0.3,Ki=0.001,Kd=0;	
	 static int err,Last_err=0;
		static float I_out=0;
	float out_put;
	 err=Target_Y-Y_mid;                //计算偏差
	I_out+=Ki*err;
 out_put=Kp*err+I_out+Kd*(err-Last_err);   //增量式PI控制器
	 Last_err=err;	                   //保存上一次偏差 
	
		if(out_put>500)
		out_put=500;
	else if (out_put<-500)
		out_put=-500;
	
	 return out_put;                         //增量输出
	
}
