#ifndef BSP_USART_H
#define BSP_USART_H
#include "stm32f4xx.h"

#define USART_RX_BUF_LENGHT     512
#define REFEREE_FIFO_BUF_LENGTH 1024

extern void usart3_init(uint8_t *rx1_buf, uint8_t *rx2_buf, uint16_t dma_buf_num);
void usart3_tx_dma_enable(uint8_t *data, uint16_t len);

#endif
