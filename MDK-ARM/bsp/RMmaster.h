#ifndef _rmmaster_H
#define _rmmaster_H
#include "chassis.h"

#include "Gimble.h"
#include "RMmaster.h"
#include "rc.h"
#include "stdlib.h"
#include "stdio.h"
#include "usart.h"
#include "RMmaster.h"
//#include "control.h"
extern ext_game_robot_state_t robot_state;//等级   功率上限   热量上限
extern ext_power_heat_data_t power_heat_data_t;//当前功率   功率缓存   枪口热量
extern ext_shoot_data_t shoot_data_t;//射速
extern uint8_t v_0[]  ;// 3  
extern uint16_t SPEED_limit;
/*超级电容代码：
					switch(Taget_V)
					{	
						case 0: dac_A=0.7  ; break;42.237
						case 1: dac_A=0.8  ; break;47.918
						case 2: dac_A=0.9  ; break;53.105
						case 3: dac_A=1    ; break;59.033
						case 4: dac_A=1.1  ; break;64.714
						case 5: dac_A=1.2  ; break;70.148
						case 6: dac_A=1.3  ; break;76.323
						case 7: dac_A=1.4  ; break;83.486
						case 8: dac_A=0.5  ; break;87.932
						case 9: dac_A=0.6  ; break;93.366
						default:dac_A=0.7  ; break;
					}
*/
void RM_limit(void);
void chasis_limit(void);
void fire_heat_limit(void);
void fire_limit(void);
void Open_chassic(void);
void freq_shooter_limit(void);


#define v0 HAL_UART_Transmit(&huart7,(uint8_t *) v_0,sizeof(v_0),  0xffff)//发送命令接通继电器  开启超级电容模式、
#define v1 HAL_UART_Transmit(&huart7,(uint8_t *) v_1,sizeof(v_1),  0xffff)
#define v2 HAL_UART_Transmit(&huart7,(uint8_t *) v_2,sizeof(v_2),  0xffff)
#define v3 HAL_UART_Transmit(&huart7,(uint8_t *) v_3,sizeof(v_3),  0xffff)
#define v4 HAL_UART_Transmit(&huart7,(uint8_t *) v_4,sizeof(v_4),  0xffff)
#define v5 HAL_UART_Transmit(&huart7,(uint8_t *) v_5,sizeof(v_5),  0xffff)
#define v6 HAL_UART_Transmit(&huart7,(uint8_t *) v_6,sizeof(v_6),  0xffff)
#define v7 HAL_UART_Transmit(&huart7,(uint8_t *) v_7,sizeof(v_7),  0xffff)
#define v8 HAL_UART_Transmit(&huart7,(uint8_t *) v_8,sizeof(v_8),  0xffff)
#define v9 HAL_UART_Transmit(&huart7,(uint8_t *) v_9,sizeof(v_9),  0xffff)
#endif






