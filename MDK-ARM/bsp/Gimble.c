#include "Gimble.h"
#include "rc.h"
#include "Remote_Control.h"
#include "pid.h"
#include "bsp_can.h"
#include "math.h"
#include "main.h"
#include "tim.h"
#include "key.h"

float USAR_6020Angle=0;
uint8_t Xnum=0;//简单的标志位来控制串级PID内外环权重比
uint8_t Xnum1=0;
int n=3;
float text[10];
float  in_XP=380;
float in_XI=0;
float  in_XD=10;
float  in_XP1=0;
float in_XI1=0;
float  in_XD1=0;
float max_angle=8192; 
float Xp=4;//700
float Xd=2;
float Xp1=0;//700
float Xd1=0;
float max_output=30000;
//uint16_t SPEED_pluss=150;//ctrl shift ??′??ó?ù???ùμ??μ   
//uint16_t SPEED=500;//3?ê??ù?è  
float mouseX_speed=0.011f;
float mouseY_speed=0.011f;
//float Y_plus=0;
//0-8000   -------------yaw?3ê±??êy?μ??D?
//float mX=0;
//float mY=0;
//int left=0;
//int right=0;
//float mouseX=0;
//float mouseY=0;


//uint16_t key_W=0;
//uint16_t key_S=0;
//uint16_t key_A=0;
//uint16_t key_D=0;
//uint16_t key_SHIFT=0;
//uint16_t key_CTRL=0;
//uint16_t key_Q=0;
//uint16_t key_E=0;
//uint16_t key_R=0;
//uint16_t key_F=0;
//uint16_t key_G=0;
//uint16_t key_Z=0;
//uint16_t key_X=0;
//uint16_t key_C=0;
//uint16_t key_V=0;
 int left1;
 int right1;
int NUM;
void Gimble_move_X(void)//yaw运动
{
//if(left==1)
//{  
				key_task();
			left1=remote_control.switch_left;
			right1=remote_control.switch_right;
    	
						if(left1==1)
						{ch[5]=ch[5]+mouseX*mouseX_speed*10;
						if(ch[5]>8192) {ch[5]=ch[5]-8192;}
						if(ch[5]<-8192){ch[5]=ch[5]+8192;}
						}
						if(left1==3)
						{
							ch[5]=1500;
						}
					if(right1==1)
	          {
								if(remote_control.mouse.press_left==1&&ch[4]==0&&NUM==0)
								{
								ch[4]=-5999;NUM=1;
								}
								if(ch[4]>-6000)
								{ch[4]+=1000;}
						
								if(ch[4]>0)
								{ch[4]=0;}
							if(remote_control.mouse.press_left==0)
								{
								NUM=0;
								}
						}
					if(right1==3)
					{
					ch[4]=0;					
					}
				  
		//	
//}

//if(left==3)	
//{
//	ch[5]= -remote_control.ch1*4096/660+1500;
	//if(ch[5]>2100)ch[5]=2100;		
//}
    Xnum++;
		if(Xnum>n){Xnum=0;	text[5]=PID(moto_chassis[5].angle,ch[5]);}//第一个PID（外环位置环）注意权重
		motor_pid[5].f_pid_reset(&motor_pid[5],in_XP,in_XI,in_XD,PID_Speed);//换成速度环了
		motor_pid[5].target =text[5];																							
		motor_pid[5].f_cal_pid(&motor_pid[5],moto_chassis[5].speed_rpm);
//    motor_pid[5].target =ch[5];																							
//		motor_pid[5].f_cal_pid(&motor_pid[5],moto_chassis[5].angle);
//																
	  motor_pid[4].target = ch[4];																					
    motor_pid[4].f_cal_pid(&motor_pid[4],moto_chassis[4].speed_rpm);

		set_moto_current(1,&hcan1,motor_pid[4].output, motor_pid[5].output,0,0);//6020   205	((float)XYZ.Yaw/360*8192)


}

void Gimble_move_Y(void)//yaw运动
{

				ch[12]=ch[12]-(mouseY*mouseY_speed*5);
	if(ch[12]>-1900){ch[12]=-1900;}
	if(ch[12]<-3200){ch[12]=-3200;}
	//if(ch[12]<-2500){ch[12]=-2500;}
	 // ch[12]=(float)(remote_control.ch2*900/1320)-2200;//max-2500 min-1600
//		Xnum++;
//		if(Xnum>n){Xnum=0;	text[4]=PID(moto_chassis[4].angle,ch[4]);}//第一个PID（外环位置环）注意权重
//		motor_pid[4].f_pid_reset(&motor_pid[4],in_XP,in_XI,in_XD,PID_Speed);//换成速度环了
//		motor_pid[4].target =text[4];																							
//		motor_pid[4].f_cal_pid(&motor_pid[4],moto_chassis[4].speed_rpm);
	
	
	Xnum1++;
		if(Xnum1>n){Xnum1=0;	text[6]=PID1(moto_chassis[12].angle,ch[12]);}//第一个PID（外环位置环）注意权重
		motor_pid[12].f_pid_reset(&motor_pid[12],in_XP1,in_XI1,in_XD1,PID_Speed);//换成速度环了
		motor_pid[12].target =text[6];																							
		motor_pid[12].f_cal_pid(&motor_pid[12],moto_chassis[12].speed_rpm);

//			motor_pid[12].target=ch[12];
//			motor_pid[12].f_cal_pid(&motor_pid[12],moto_chassis[12].angle);
		set_moto_current(1,&hcan2,0, motor_pid[12].output,0,0);//6020   205	


}

float PID(float now,float target)
{

	 static int err=0;

	float out_put;
	if(target<0) target=8191+target;//不加的话电机会乱转
	
	 err=target-now;                //计算偏差
	
		if( err>(max_angle/2)){  err= err-max_angle;}//解决360度问题让电机走最优路径
	else if( err<-(max_angle/2)){ err= err+max_angle;}
	
 out_put=Xp/100*err-Xd/100*(moto_chassis[5].speed_rpm);   //PI控制器

  
//	if(out_put>max_output)out_put=max_output;
//	else if(out_put<-max_output)out_put=-max_output;//限制输出，加上后改max_output可调节电机回弹的力气

  
	 return out_put;                         //输出
	
}
float PID1(float now1,float target1)
{

	 static int err1=0;

	float out_put1;
	if(target1<0) target1=8191+target1;//不加的话电机会乱转
	
	 err1=target1-now1;                //计算偏差
	
		if( err1>(max_angle/2)){  err1= err1-max_angle;}//解决360度问题让电机走最优路径
	else if( err1<-(max_angle/2)){ err1= err1+max_angle;}
	
 out_put1=Xp1/100*err1-Xd1/100*(moto_chassis[12].speed_rpm);   //PI控制器

  
//	if(out_put>max_output)out_put=max_output;
//	else if(out_put<-max_output)out_put=-max_output;//限制输出，加上后改max_output可调节电机回弹的力气

  
	 return out_put1;                         //输出
	
}
		

		
