#ifndef _chassic_H
#define _chassic_H
#include "bsp_can.h"
#include "chassis.h"
#include "Remote_Control.h"
//#include "Gimble.h"
extern float mouseX;
extern float mouseY;
extern uint16_t key_W;
extern uint16_t key_S;
extern uint16_t key_A;
extern uint16_t key_D;
extern uint16_t key_SHIFT;
extern uint16_t key_CTRL;
extern uint16_t key_Q;
extern uint16_t key_E;
extern uint16_t key_R;
extern uint16_t key_F;
extern uint16_t key_G;
extern uint16_t key_Z;
extern uint16_t key_X;
extern uint16_t key_C;
extern uint16_t key_V;
extern uint16_t SPEED;

void chassis_move1(void);//自由运动
void chassis_move2(void);//全向运动
void  chassis_enable(void);//底盘使能
//extern uint16_t key_W;
void  chassis_Follow(uint16_t taget_ang);//底盘跟随
float dipan_pid (uint16_t angle,uint16_t taget_angle); 
#endif

