#include "chassis.h"
#include "rc.h"
#include "Remote_Control.h"
#include "pid.h"
#include "bsp_can.h"
#include "math.h"
#include "main.h"
#include "tim.h" 
#include "key.h"
#include "cmsis_os.h"
float rotor_Angle=0;//已经旋转的角度
uint16_t rotor_V=1000;//8000  遥控器速度最大值
float Bchang=1500;//云台补偿值（矫正角度）

#define My_Pi  3.1415f
int controlleft=0;
int controlright=0;

float Vx=0;
float Vy=0;
float Vz=0;
float dipan_return_maxspeed=1000;
float mouseX_speed1=0.011f;

int check=0;

void chassis_move1(void)//自由运动
{

	key_task();
	controlleft=remote_control.switch_left;
  controlright=remote_control.switch_right;
	//SPEED=1500;
//	if(key_SHIFT==1&&key_CTRL==0) {SPEED=2000;}
//	if(key_CTRL==1&&key_SHIFT==0) {SPEED=1000;}
//	
	if(check){
		if(key_SHIFT==1&&key_CTRL==0) {
			check--;
			if(SPEED<2000){
				SPEED+=250;
			}
		}
		if(key_CTRL==1&&key_SHIFT==0) {
			check--;
			if(SPEED>500){
				SPEED-=250;
			}	
		}
	}
	if(!check){
		if(key_SHIFT==0&&key_CTRL==0) {
			check++;
		}
	}
	
//	ch[0]= key_W*rotor_V
	if(controlleft==1)
	{
		   ch[0]= (-key_W*SPEED)+(key_S*SPEED)+(key_D*SPEED)+(-key_A*SPEED)+(-key_Q*SPEED)+(key_E*SPEED);
			 ch[1]= (key_W*SPEED)+(-key_S*SPEED)+(key_D*SPEED)+(-key_A*SPEED)+(-key_Q*SPEED)+(key_E*SPEED);
			 ch[2]= (key_W*SPEED)+(-key_S*SPEED)+(-key_D*SPEED)+(key_A*SPEED)+(-key_Q*SPEED)+(key_E*SPEED);
			 ch[3]= (-key_W*SPEED)+(key_S*SPEED)+(-key_D*SPEED)+(key_A*SPEED)+(-key_Q*SPEED)+(key_E*SPEED);
	
	}
	
	if(controlleft==2)
	{
	    ch[0]= -remote_control.ch4*4000/660+remote_control.ch3*4000/660+remote_control.ch1*4000/660;//    (ch4>0)=w ch3
			 ch[1]= +remote_control.ch4*4000/660+remote_control.ch3*4000/660+remote_control.ch1*4000/660;
			 ch[2]= +remote_control.ch4*4000/660-remote_control.ch3*4000/660+remote_control.ch1*4000/660;
			 ch[3]= -remote_control.ch4*4000/660-remote_control.ch3*4000/660+remote_control.ch1*4000/660;
	}
	if(controlleft==3)
	{
		
//		 rotor_Angle=(-moto_chassis[5].angle+Bchang)*360/8192;
//		
//		  	Vx=-cos(rotor_Angle/180*My_Pi)*(key_D*SPEED-key_A*SPEED)+sin(rotor_Angle/180*My_Pi)*(key_W*SPEED-key_S*SPEED);
//				Vy=-cos(rotor_Angle/180*My_Pi)*(key_W*SPEED-key_S*SPEED)-sin(rotor_Angle/180*My_Pi)*(key_D*SPEED-key_A*SPEED); 
//				
//		chassis_Follow(Bchang);
//				
//				
//			ch[0]= -Vy+Vx-Vz;
//			ch[1]= -Vy-Vx-Vz;
//			ch[2]=  Vy-Vx-Vz;
//			ch[3]=  Vy+Vx-Vz;
		
		ch[0]= 0;
		ch[1]= 0;
		ch[2]= 0;
		ch[3]= 0;
		
		
		
		
//		
//		 Vz=mouseX*mouseX_speed1*800;
//		
//		  ch[0]= (-key_W*SPEED)+(key_S*SPEED)+(key_D*SPEED)+(-key_A*SPEED)+Vz;
//			 ch[1]= (key_W*SPEED)+(-key_S*SPEED)+(key_D*SPEED)+(-key_A*SPEED)+Vz;
//			 ch[2]= (key_W*SPEED)+(-key_S*SPEED)+(-key_D*SPEED)+(key_A*SPEED)+Vz;
//			 ch[3]= (-key_W*SPEED)+(key_S*SPEED)+(-key_D*SPEED)+(key_A*SPEED)+Vz;
//		
//		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}	
		for(int i=0; i<4; i++)
    {
      motor_pid[i].target = ch[i]; 																							
      motor_pid[i].f_cal_pid(&motor_pid[i],moto_chassis[i].speed_rpm);    //3506 moto_chassis[0].speed_rpm
    }
		//201 202 203 204
			set_moto_current(0,&hcan1,motor_pid[0].output,motor_pid[1].output,motor_pid[2].output,motor_pid[3].output);//3506
 }

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
// void  chassis_Follow(uint16_t taget_ang)//底盘跟随
//{
//		if(moto_chassis[5].angle<(taget_ang-100)||moto_chassis[5].angle>(taget_ang+100))		//100是允许的误差，否则容易左右震荡	

//		
//		Vz=dipan_pid(moto_chassis[5].angle,taget_ang);

//	  else Vz=0;

//}
//		 float dipan_pid (uint16_t angle,uint16_t taget_angle)    //////////////////////底盘归位
//{
//	
//   float Kp=-1.5,Ki=0,Kd=0;	
//	 static int err,Last_err=0;
//		static float I_out=0;
//	float out_put;

//	 err=angle-taget_angle;                //计算偏差
//	if( err>4096){  err= err-8191;}
//	else if( err<-4096){ err= err+8191;}
//	I_out+=Ki*err;
// out_put=Kp*err+I_out+Kd*(err-Last_err);   //增量式PI控制器
//	 Last_err=err;	                   //保存上一次偏差 
//	
//	if     (out_put>dipan_return_maxspeed)out_put=dipan_return_maxspeed;
//	else if(out_put<-dipan_return_maxspeed)out_put=-dipan_return_maxspeed;
//	
//	 return out_put;                         //增量输出
//	
//}



