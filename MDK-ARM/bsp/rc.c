//#include "main.h"
#include "rc.h"
#include "CRC8_CRC16.h"
#include "fifo.h"
#include "bsp_usart.h"

extern UART_HandleTypeDef huart3;

uint8_t usart3_buf[2][USART_RX_BUF_LENGHT];

fifo_s_t referee_fifo;
uint8_t referee_fifo_buf[REFEREE_FIFO_BUF_LENGTH];
unpack_data_t referee_unpack_obj;

frame_header_struct_t referee_receive_header;
frame_header_struct_t referee_send_header;

ext_game_state_t game_state;
ext_game_result_t game_result;
ext_game_robot_HP_t game_robot_HP_t;

ext_event_data_t field_event;
ext_supply_projectile_action_t supply_projectile_action_t;
ext_supply_projectile_booking_t supply_projectile_booking_t;
ext_referee_warning_t referee_warning_t;


ext_game_robot_state_t robot_state;//等级   功率上限   热量上限

ext_power_heat_data_t power_heat_data_t;//当前功率   功率缓存   枪口热量

ext_game_robot_pos_t game_robot_pos_t;
ext_buff_musk_t buff_musk_t;
aerial_robot_energy_t robot_energy_t;
ext_robot_hurt_t robot_hurt_t;
ext_shoot_data_t shoot_data_t;//射速
ext_bullet_remaining_t bullet_remaining_t;
ext_student_interactive_data_t student_interactive_data_t;
ext_senddata_t ui_data;//ui界面

void referee_data_solve(uint8_t *frame)
{
    uint16_t cmd_id = 0;

    uint8_t index = 0;

    memcpy(&referee_receive_header, frame, sizeof(frame_header_struct_t));

    index += sizeof(frame_header_struct_t);

    memcpy(&cmd_id, frame + index, sizeof(uint16_t));
    index += sizeof(uint16_t);

    switch (cmd_id)
    {
        case GAME_STATE_CMD_ID:
        {
            memcpy(&game_state, frame + index, sizeof(ext_game_state_t));
        }
        break;
        case GAME_RESULT_CMD_ID:
        {
            memcpy(&game_result, frame + index, sizeof(game_result));
        }
        break;
        case GAME_ROBOT_HP_CMD_ID:
        {
            memcpy(&game_robot_HP_t, frame + index, sizeof(ext_game_robot_HP_t));
        }
        break;


        case FIELD_EVENTS_CMD_ID:
        {
            memcpy(&field_event, frame + index, sizeof(field_event));
        }
        break;
        case SUPPLY_PROJECTILE_ACTION_CMD_ID:
        {
            memcpy(&supply_projectile_action_t, frame + index, sizeof(supply_projectile_action_t));
        }
        break;
        case SUPPLY_PROJECTILE_BOOKING_CMD_ID:
        {
            memcpy(&supply_projectile_booking_t, frame + index, sizeof(supply_projectile_booking_t));
        }
        break;
        case REFEREE_WARNING_CMD_ID:
        {
            memcpy(&referee_warning_t, frame + index, sizeof(ext_referee_warning_t));
        }
        break;

        case ROBOT_STATE_CMD_ID:
        {
            memcpy(&robot_state, frame + index, sizeof(robot_state));
        }
        break;
        case POWER_HEAT_DATA_CMD_ID:
        {
            memcpy(&power_heat_data_t, frame + index, sizeof(power_heat_data_t));
        }
        break;
        case ROBOT_POS_CMD_ID:
        {
            memcpy(&game_robot_pos_t, frame + index, sizeof(game_robot_pos_t));
        }
        break;
        case BUFF_MUSK_CMD_ID:
        {
            memcpy(&buff_musk_t, frame + index, sizeof(buff_musk_t));
        }
        break;
        case AERIAL_ROBOT_ENERGY_CMD_ID:
        {
            memcpy(&robot_energy_t, frame + index, sizeof(robot_energy_t));
        }
        break;
        case ROBOT_HURT_CMD_ID:
        {
            memcpy(&robot_hurt_t, frame + index, sizeof(robot_hurt_t));
        }
        break;
        case SHOOT_DATA_CMD_ID:
        {
            memcpy(&shoot_data_t, frame + index, sizeof(shoot_data_t));
        }
        break;
        case BULLET_REMAINING_CMD_ID:
        {
            memcpy(&bullet_remaining_t, frame + index, sizeof(ext_bullet_remaining_t));
        }
        break;
        case STUDENT_INTERACTIVE_DATA_CMD_ID:
        {
            memcpy(&student_interactive_data_t, frame + index, sizeof(student_interactive_data_t));
        }
        break;
        default:
        {
            break;
        }
    }
}

void referee_unpack_fifo_data(void)
{
  uint8_t byte = 0;
  uint8_t sof = HEADER_SOF;
  unpack_data_t *p_obj = &referee_unpack_obj;

  while ( fifo_s_used(&referee_fifo) )
  {
    byte = fifo_s_get(&referee_fifo);
    switch(p_obj->unpack_step)
    {
      case STEP_HEADER_SOF:
      {
        if(byte == sof)
        {
          p_obj->unpack_step = STEP_LENGTH_LOW;
          p_obj->protocol_packet[p_obj->index++] = byte;
        }
        else
        {
          p_obj->index = 0;
        }
      }break;
      
      case STEP_LENGTH_LOW:
      {
        p_obj->data_len = byte;
        p_obj->protocol_packet[p_obj->index++] = byte;
        p_obj->unpack_step = STEP_LENGTH_HIGH;
      }break;
      
      case STEP_LENGTH_HIGH:
      {
        p_obj->data_len |= (byte << 8);
        p_obj->protocol_packet[p_obj->index++] = byte;

        if(p_obj->data_len < (REF_PROTOCOL_FRAME_MAX_SIZE - REF_HEADER_CRC_CMDID_LEN))
        {
          p_obj->unpack_step = STEP_FRAME_SEQ;
        }
        else
        {
          p_obj->unpack_step = STEP_HEADER_SOF;
          p_obj->index = 0;
        }
      }break;
      case STEP_FRAME_SEQ:
      {
        p_obj->protocol_packet[p_obj->index++] = byte;
        p_obj->unpack_step = STEP_HEADER_CRC8;
      }break;

      case STEP_HEADER_CRC8:
      {
        p_obj->protocol_packet[p_obj->index++] = byte;

        if (p_obj->index == REF_PROTOCOL_HEADER_SIZE)
        {
          if ( verify_CRC8_check_sum(p_obj->protocol_packet, REF_PROTOCOL_HEADER_SIZE) )
          {
            p_obj->unpack_step = STEP_DATA_CRC16;
          }
          else
          {
            p_obj->unpack_step = STEP_HEADER_SOF;
            p_obj->index = 0;
          }
        }
      }break;  
      
      case STEP_DATA_CRC16:
      {
        if (p_obj->index < (REF_HEADER_CRC_CMDID_LEN + p_obj->data_len))
        {
           p_obj->protocol_packet[p_obj->index++] = byte;  
        }
        if (p_obj->index >= (REF_HEADER_CRC_CMDID_LEN + p_obj->data_len))
        {
          p_obj->unpack_step = STEP_HEADER_SOF;
          p_obj->index = 0;

          if ( verify_CRC16_check_sum(p_obj->protocol_packet, REF_HEADER_CRC_CMDID_LEN + p_obj->data_len) )
          {
            referee_data_solve(p_obj->protocol_packet);
          }
        }
      }break;

      default:
      {
        p_obj->unpack_step = STEP_HEADER_SOF;
        p_obj->index = 0;
      }break;
    }
  }
}
void referee_data_send(ext_senddata_t data)
{
	static uint8_t seq=0;
	uint8_t tx_buf[128];
	data.tx_header.SOF=0xa5;
	data.tx_header.data_length=sizeof(data.cilentdata_header)+sizeof(data.clientdata);
	data.tx_header.seq=seq;
	if(++seq==0xff)seq=0;
	memcpy(tx_buf,&data,5);
	append_CRC8_check_sum(tx_buf,5);
	data.tx_header.CRC8=tx_buf[4];
	data.cmdid=0x0301;
	memcpy(tx_buf,&data,sizeof(ext_senddata_t));
	append_CRC16_check_sum(tx_buf,sizeof(ext_senddata_t));
	usart3_tx_dma_enable(tx_buf,sizeof(ext_senddata_t));
	HAL_UART_Transmit(&huart3,tx_buf,sizeof(ext_senddata_t),0xff);
}
void ui_draw(uint16_t data_cmd_id,uint16_t receiver_ID,uint16_t send_ID,uint8_t graphic_name,uint32_t operate_tpye,uint32_t graphic_tpye,
	uint32_t layer,uint32_t color,uint32_t start_angle,uint32_t end_angle,uint32_t width,uint32_t start_x,uint32_t end_x,uint32_t radius,uint32_t start_y,uint32_t end_y)
{
	static uint8_t seq=0;uint8_t tx_buf[128];
	ui_data.cilentdata_header.data_cmd_id=data_cmd_id;
	ui_data.cilentdata_header.receiver_ID=receiver_ID;
	ui_data.cilentdata_header.send_ID=send_ID;
	ui_data.clientdata.graphic_name[0]=graphic_name;
	ui_data.clientdata.operate_tpye=operate_tpye;
	ui_data.clientdata.graphic_tpye=graphic_tpye;
	ui_data.clientdata.layer=layer;
	ui_data.clientdata.color=color;
	ui_data.clientdata.start_angle=start_angle;
	ui_data.clientdata.end_angle=end_angle;
	ui_data.clientdata.width=width;
	ui_data.clientdata.start_x=start_x;
	ui_data.clientdata.end_x=end_x;
	ui_data.clientdata.radius=radius;
	ui_data.clientdata.start_y=start_y;
	ui_data.clientdata.end_y=end_y;
//	referee_data_send(ui_data);
	
	ui_data.tx_header.SOF=0xa5;
	ui_data.tx_header.data_length=sizeof(ui_data.cilentdata_header)+sizeof(ui_data.clientdata);
	ui_data.tx_header.seq=seq;
	if(++seq==0xff)seq=0;
	memcpy(tx_buf,&ui_data,5);
	append_CRC8_check_sum(tx_buf,5);
	ui_data.tx_header.CRC8=tx_buf[4];
	ui_data.cmdid=0x0301;
	memcpy(tx_buf,&ui_data,sizeof(ext_senddata_t));
	append_CRC16_check_sum(tx_buf,sizeof(ext_senddata_t));
	usart3_tx_dma_enable(tx_buf,sizeof(ext_senddata_t));
	HAL_UART_Transmit(&huart3,tx_buf,sizeof(ext_senddata_t),0xff);
}
