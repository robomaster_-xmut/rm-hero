/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "rc.h"
#include "Ammo-Booster.h"
#include "chassis.h"
#include "Gimble.h"
#include "tim.h"
//裁判系统的头文件
#include "bsp_usart.h"
#include "fifo.h"
#include "crc8_crc16.h"
#include "rc.h"
#include "RMmaster.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId myTask02Handle;
osThreadId myTask03Handle;
osThreadId myTask04Handle;
osThreadId myTask05Handle;
osThreadId myTask06Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
extern int modeflag;
extern int magazineflag1;
int chassisflag=0;
int chassis_flag=1;
int gimbleflag=0;
int gimble_flag=1;
int boosterflag=0;
int booster_flag=1;
int flag_level=1;
int level_int=0;
int i1=0,i2=0,i3=0,i4=0,i5=0;
int check1=1;
int check2=1;
int check3=1;
//裁判系统
extern UART_HandleTypeDef huart3;
extern uint8_t usart3_buf[2][USART_RX_BUF_LENGHT];
extern fifo_s_t referee_fifo;
extern uint8_t referee_fifo_buf[REFEREE_FIFO_BUF_LENGTH];
int chassispower = 0;
int gimblepower =0;
int boosterpower = 0;
int flag_exchange=1;
/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartTask02(void const * argument);
void StartTask03(void const * argument);
void StartTask04(void const * argument);
void StartTask05(void const * argument);
void StartTask06(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTask02 */
  osThreadDef(myTask02, StartTask02, osPriorityIdle, 0, 128);
  myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

  /* definition and creation of myTask03 */
  osThreadDef(myTask03, StartTask03, osPriorityIdle, 0, 128);
  myTask03Handle = osThreadCreate(osThread(myTask03), NULL);

  /* definition and creation of myTask04 */
  osThreadDef(myTask04, StartTask04, osPriorityIdle, 0, 128);
  myTask04Handle = osThreadCreate(osThread(myTask04), NULL);

  /* definition and creation of myTask05 */
  osThreadDef(myTask05, StartTask05, osPriorityIdle, 0, 128);
  myTask05Handle = osThreadCreate(osThread(myTask05), NULL);

  /* definition and creation of myTask06 */
  osThreadDef(myTask06, StartTask06, osPriorityIdle, 0, 128);
  myTask06Handle = osThreadCreate(osThread(myTask06), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
		fifo_s_init(&referee_fifo, referee_fifo_buf, REFEREE_FIFO_BUF_LENGTH);
    usart3_init(usart3_buf[0], usart3_buf[1], USART_RX_BUF_LENGHT);
		 while(1)
    {

        referee_unpack_fifo_data();//裁判系统返回参数线程
        osDelay(10);
			
    }
//    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void const * argument)
{
  /* USER CODE BEGIN StartTask02 */
  /* Infinite loop */
  for(;;)
  {
fifo_s_init(&referee_fifo, referee_fifo_buf, REFEREE_FIFO_BUF_LENGTH);
    usart3_init(usart3_buf[0], usart3_buf[1], USART_RX_BUF_LENGHT);
		 while(1)
    {

        referee_unpack_fifo_data();//裁判系统返回参数线程
        osDelay(10);
			
    }
  }
  /* USER CODE END StartTask02 */
}

/* USER CODE BEGIN Header_StartTask03 */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask03 */
void StartTask03(void const * argument)
{
  /* USER CODE BEGIN StartTask03 */
  /* Infinite loop */
  for(;;)
  {
	
	if(chassispower==1)
	{chassis_move1();}

   osDelay(1);
//	if(boosterpower==1&check3==1)
//	{
	//check3=0;	
	//booster();	
//	}
	// osDelay(3000);
  }
  /* USER CODE END StartTask03 */
}

/* USER CODE BEGIN Header_StartTask04 */
/**
* @brief Function implementing the myTask04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask04 */
void StartTask04(void const * argument)
{
  /* USER CODE BEGIN StartTask04 */
  /* Infinite loop */
  for(;;)
  {
	//	int i1=0,i2=0,i3=0,i4=0,i5=0;
		 
//			if(robot_state.robot_level==2)
//		{
//			level[2]=0x0B;
//		HAL_UART_Transmit_DMA(&huart6,level,sizeof(level));
//		}
//			if(robot_state.robot_level==3)
//		{
//			level[2]=0x0C;
//		HAL_UART_Transmit_DMA(&huart6,level,sizeof(level));
//		}
		//Gimble_move_Y();
	//	Gimble_move_X();
	//	booster();
		
		 if(gimblepower==1)
	{
		Gimble_move_X();
  	Gimble_move_Y();
  }
		
		if(boosterpower==1)
		{
		booster ();
		}
		osDelay(1)
;		
		
		
  } 
  /* USER CODE END StartTask04 */
}

/* USER CODE BEGIN Header_StartTask05 */
/**
* @brief Function implementing the myTask05 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask05 */
void StartTask05(void const * argument)
{
  /* USER CODE BEGIN StartTask05 */
  /* Infinite loop */
  for(;;)
  {
  
	//	buf[2]=0;			
		//while(buf[2]==0)
	//HAL_UART_Transmit_DMA(&huart6,buff,sizeof(buff));
		//osDelay(4000);
			//HAL_UART_Transmit_DMA(&huart6,buf,sizeof(buf));
	
		
		
		chassisflag = robot_state.mains_power_chassis_output;
		if(chassisflag == 1&&chassis_flag == 1)
		{		
		osDelay(3000);
		chassispower = 1;
		chassis_flag = 0;
		}
			
		if(gimbleflag == 1&&gimble_flag ==1){
		osDelay(3000);
		gimblepower=1;
		gimble_flag=0;}	
	
			
		boosterflag = robot_state.mains_power_shooter_output;
		if(boosterflag == 1&&booster_flag ==1){
		osDelay(3000);
		boosterpower=1;
		booster_flag=0;}
//		
   osDelay(1);
  }
  /* USER CODE END StartTask05 */
}

/* USER CODE BEGIN Header_StartTask06 */
/**
* @brief Function implementing the myTask06 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask06 */
void StartTask06(void const * argument)
{
  /* USER CODE BEGIN StartTask06 */
  /* Infinite loop */
  for(;;)
  {
		
		uint8_t buff[]={0x0a,0x0d,0x30,0x0d,0x0a}; 
		uint8_t buf[]={0x0a,0x0d,0x31,0x0d,0x0a}; 	
		uint8_t level[]={0x0a,0x0d,0x0A,0x0d,0x0a};
		
				level_int=robot_state.robot_level;
			 gimbleflag = robot_state.mains_power_gimbal_output;
		if(gimbleflag ==0 &&i1==0 ){
			for(;i1<5;i1++)
		{HAL_UART_Transmit_DMA(&huart6,buff,sizeof(buff));
			osDelay(500);
		}
		}
		if(gimbleflag ==1&&i2==0 ){
				for(;i2<5;i2++)
		{HAL_UART_Transmit_DMA(&huart6,buf,sizeof(buff));
			osDelay(500);
			level_int=robot_state.robot_level;
		}
		}
   
		if(level_int==1&&i3==0)
		{
			for(;i3<5;i3++)
		{HAL_UART_Transmit_DMA(&huart6,level,sizeof(level));
		osDelay(500);
		}
			level_int=robot_state.robot_level;
		}
		
		if(level_int==2&&i4==0)
		{
			level[2]=0x0B;
			for(;i4<5;i4++)
		{HAL_UART_Transmit_DMA(&huart6,level,sizeof(level));
			osDelay(500);
		}
				level_int=robot_state.robot_level;
		}
	
		if(level_int==3&&i5==0)
		{

			level[2]=0x0C;
		for(;i5<5;i5++)
			{HAL_UART_Transmit_DMA(&huart6,level,sizeof(level));
			osDelay(500);
			}
			level_int=robot_state.robot_level;
		}
    //		ui_char_data.cilentdata_header.data_cmd_id=0x0110;//绘制字符
//		ui_char_data.cilentdata_header.receiver_ID=0x0104;
//		ui_char_data.cilentdata_header.send_ID=4;
//		ui_char_data.clientdata.graphic_name[0]=14;
//		ui_char_data.clientdata.operate_tpye=1;
//		ui_char_data.clientdata.graphic_tpye=7;
//		ui_char_data.clientdata.layer=2;
//		ui_char_data.clientdata.color=0;
//		ui_char_data.clientdata.start_angle=20;//字体大小
//		ui_char_data.clientdata.end_angle=3;//字符长度
//		ui_char_data.clientdata.width=2;//线条宽度
//		ui_char_data.clientdata.start_x=10;//起点 x 坐标
//		ui_char_data.clientdata.end_x=0;
//		ui_char_data.clientdata.radius=0;
//		ui_char_data.clientdata.start_y=850;//起点 y 坐标
//		ui_char_data.clientdata.end_y=0;
//		memcpy(ui_char_data.chardata,"PFA",3);
//		referee_uichar_data_send(ui_char_data);
		ui_draw(0x0101,0x0104,4,1,1,0,2,0,0,0,2,960,960,0,640,140);//画UI
		ui_draw(0x0101,0x0104,4,2,1,0,2,0,0,0,2,905,1005,0,540,540);
		ui_draw(0x0101,0x0104,4,3,1,0,2,0,0,0,2,910,1000,0,500,500);
//		ui_draw(0x0101,0x0104,4,4,1,0,2,0,0,0,2,915,995,0,460,460);
		ui_draw(0x0101,0x0104,4,5,1,0,2,0,0,0,2,920,990,0,420,420);
		ui_draw(0x0101,0x0104,4,1,1,0,1,0,0,0,2,925,985,0,380,380);
    osDelay(500);
  }
  /* USER CODE END StartTask06 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void referee_uichar_data_send(ext_sendchardata_t data)
{
	static uint8_t sq=0;
	uint8_t tx_buf[128];
	data.tx_header.SOF=0xa5;
	data.tx_header.data_length=sizeof(data.cilentdata_header)+sizeof(data.clientdata)+sizeof(data.chardata);
	data.tx_header.seq=sq;
	if(++sq==0xff)sq=0;
	memcpy(tx_buf,&data,5);
	append_CRC8_check_sum(tx_buf,5);
	data.tx_header.CRC8=tx_buf[4];
	data.cmdid=0x0301;
	memcpy(tx_buf,&data,sizeof(ext_sendchardata_t));
	append_CRC16_check_sum(tx_buf,sizeof(ext_sendchardata_t));
	HAL_UART_Transmit(&huart3,tx_buf,sizeof(ext_sendchardata_t),0xff);
	
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
